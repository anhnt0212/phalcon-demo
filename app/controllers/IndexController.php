<?php
namespace Phalcons\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcons\Models\User;
/**
 * Display the default index page.
 */
class IndexController extends Controller
{
    public function indexAction()
    {

    }
    public function loginAction(){
    	if($frm =$this->request->getPost()){
            $email    = $this->request->getPost('email');
            $password = $this->request->getPost('password');
            $user = User::findFirstByEmail($email);
            if ($user) {
                if ($this->security->checkHash($password, $user->passwords)) {
                   $this->flashSession->success("Đăng nhập thành công!");
                   $this->session->set("user-email",$user->email);
                   return $this->response->redirect("index/detail");
                }
            }
            $this->flashSession->error("Email hoặc password chưa chính xác");
            return $this->response->redirect("index/login");
        }
    }
    public function registerAction(){

        $user = new User();
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $users = User::findFirstByEmail($email);
        if($users){
           echo "email ton tai";
        }
        else{
            $user->email = $email;
            $user->passwords = $this->security->hash($password);
            $user->save();
            if($user->save()){
              echo "dang ki thanh cong"; 
            }
            else{
                echo "co loi xay ra";
            }
        }
       
    }
    public function detailAction(){
        if ($this->session->has("user-email")) {
            $name = $this->session->get("user-email");
            echo "xin chao ban ".$name." ";
            die();
        }
    }
}
