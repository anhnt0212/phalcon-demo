<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Register</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Administrator" />
<?php echo $this->tag->stylesheetLink('/admin/avant/assets/css/styles.min.css'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="focusedform">

<div class="verticalcenter">
  <div class="panel panel-primary">

    <form action="/index/register" method="post" id="frmRegistry" name = "frmRegistry" class="form-horizontal" style="margin-bottom: 0px !important;" />
    <div class="panel-body">
      <h4 class="text-center" style="margin-bottom: 25px;">Register</h4>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-user"></i></span>
                            <input type="email" class="form-control" name="email" placeholder="Email" autofocus required class="form-control parsley-validated" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-lock"></i></span>
                            <input type="password" class="form-control" id="password"  name="password" placeholder="Password" autofocus required class="form-control parsley-validated" />
                        </div>
                    </div>
                </div>
    </div>
    <div class="panel-footer">
      <div class="pull-right">
        <a href="#" class="btn btn-default">Clear</a>
        <input type="submit" name="submit" value="Register" class="btn btn-primary" />
      </div>
    </div>
    </form>
  </div>
 </div>
</body>
</html>
